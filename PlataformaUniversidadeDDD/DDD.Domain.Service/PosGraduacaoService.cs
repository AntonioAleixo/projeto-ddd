﻿﻿using DDD.Domain.PicContext;
using DDD.Infra.SQLServer.Interfaces;

namespace DDD.Domain.Service
{
    public class PosGraduacaoService
    {

        readonly IPosGraduacaoRepository _posGraduacaoRepository;
        readonly IProjetoRepository _projetoRepository;
        readonly IPesquisadorRepository _pesquisadorRepository;

        public PosGraduacaoService(IPosGraduacaoRepository posGraduacaoRepository, IProjetoRepository projetoRepository, IPesquisadorRepository pesquisadorRepository)
        {

            _posGraduacaoRepository = posGraduacaoRepository;
            _pesquisadorRepository = pesquisadorRepository;
            _projetoRepository = projetoRepository;

        }

     
        public bool cadastrarPosGraduacao(int idProjeto, int idPesquisador)
        {
            try
            {
                var projeto = _projetoRepository.GetProjetoById(idProjeto);
                if (projeto.AnosDuracao >= 1)
                {
                    var pesquisador = _pesquisadorRepository.GetPesquisadorById(idPesquisador);
                    if (pesquisador.Titulacao != TitulacaoEnum.POS_GRADUADO)
                    {
                        if (projeto != null)
                        {
                            _posGraduacaoRepository.InsertPosGraduacao(projeto.ProjetoId, idPesquisador);
                        }
                    }

                }
                return true;
            }
            catch (Exception ex)
            {
                var erro = ex.Message;
                throw;
            }
           

        }


    }
}