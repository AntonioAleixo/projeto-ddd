﻿using DDD.Domain.PicContext;
using DDD.Domain.SecretariaContext;
using DDD.Domain.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DDD.Application.Service
{
    public class ApplicationServiceProjeto
    {
        readonly PosGraduacaoService _posGraduacaoService;

        public ApplicationServiceProjeto(PosGraduacaoService posGraduacaoService)
        {
            _posGraduacaoService = posGraduacaoService;
        }

        public void GerarPosGraduacao(int idProjeto, int idPesquisador)
        {
            var PosGraduacao = _posGraduacaoService.cadastrarPosGraduacao(idProjeto, idPesquisador);
            if (PosGraduacao)
            {
                //Enviar email
            }
        }
    }
}