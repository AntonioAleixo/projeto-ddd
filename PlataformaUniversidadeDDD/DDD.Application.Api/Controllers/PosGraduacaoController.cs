﻿using DDD.Application.Service;
using DDD.Domain.PicContext;
using DDD.Domain.Service;
using DDD.Infra.SQLServer.Interfaces;
using Microsoft.AspNetCore.Mvc;

namespace DDD.Application.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PosGraduacaoController : ControllerBase
    {
        readonly IPosGraduacaoRepository _posGraduacaoRepository;
        readonly ApplicationServiceProjeto _posGraduacaoService;

        public PosGraduacaoController(IPosGraduacaoRepository posGraduacaoRepository, ApplicationServiceProjeto applicationServiceProjeto)
        {
            _posGraduacaoRepository = posGraduacaoRepository;
            _posGraduacaoService = applicationServiceProjeto;
        }

        [HttpGet("{id}")]
        public ActionResult<PosGraduacao> GetPosGraduacaoById(int id)
        {
            return Ok(_posGraduacaoRepository.GetPosGraduacaoById(id));
        }

        [HttpPost]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [HttpPost]
        [Route("criarProjeto")]
        public void CreatePosGraduacao(int idProjeto, int idPesquisador)
        {
            _posGraduacaoService.GerarPosGraduacao(idProjeto, idPesquisador);

        }
    }
}
