﻿using DDD.Application.Service;
using DDD.Domain.PicContext;
using DDD.Domain.SecretariaContext;
using DDD.Infra.SQLServer.Interfaces;
using Microsoft.AspNetCore.Mvc;

namespace DDD.Application.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProjetoController : ControllerBase
    {
        readonly IProjetoRepository _projetoRepository;

        public ProjetoController(IProjetoRepository ProjetoRepository)
        {
            _projetoRepository = ProjetoRepository;
        }

        [HttpGet("{id}")]
        public ActionResult<Projeto> GetProjetoById(int id)
        {
            return Ok(_projetoRepository.GetProjetoById(id));
        }

        [HttpPost]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public ActionResult<Projeto> insertProjeto(Projeto projeto)
        {
            if (projeto.ProjetoName.Length < 3 || projeto.ProjetoName.Length > 30)
            {
                return BadRequest("Nome deve ser maior que 3 e menor que 30 caracteres.");
            }
            _projetoRepository.insertProjeto(projeto);
            return CreatedAtAction(nameof(GetProjetoById), new { id = projeto.ProjetoId}, projeto);
        }
    }
}
